# Resume Builder

This is an app that converts a formatted markdown to styled resume.

## Markdown format

The resume starts with the person's name with short introduction text if wants to.

Then eash section is separated with second level heading and following sections are supported:

    - Biography
    - Experiences
    - Skills
    - Educations

And then in each section, details are listed with markdown list.

## Example
```
# Canditate's name

## Biography
- Email: email@email.com
- Role: Software Engineer
- Blog: www.google.com

## Experiences
- First job
  - From: 2018
  - To: 2020
  - Description...long text
```

