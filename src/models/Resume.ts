export default class Resume {
  name: string;
  role: string;
  sections: Section[];

  constructor(args: { name: string; role: string; sections: Section[] }) {
    this.name = args.name;
    this.role = args.role;
    this.sections = args.sections;
  }
}

export class Section {
  type: SectionType;
  title: string;
  content: string | KeyValue | ContentTitleBulletList[];

  constructor(args: {
    type: SectionType;
    title: string;
    content: string | KeyValue | ContentTitleBulletList[];
  }) {
    this.type = args.type;
    this.title = args.title;
    this.content = args.content;
  }
}

export type SectionType = "summary" | "experiences" | "educations" | "biography" | "skills";

export type KeyValue = { [key: string]: string };

export type ContentTitleBulletList = { contentTitle: ContentTitle; bulletList: BulletList };
export type ContentTitle = { title: string; period: Period };
export type Period = { start: string; end: string };
export type BulletList = { title: string; list: string[] };
