import React from "react";

import "./AppHeader.scss";

interface AppHeaderHandlers {
  onEditButtonClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

const AppHeader: React.FC<AppHeaderHandlers> = (props) => {
  return (
    <div className="app-header">
      <div className="title">Resuuume</div>
      <div className="edit-button" onClick={props.onEditButtonClick}>
        Edit
      </div>
    </div>
  );
};

export default AppHeader;
