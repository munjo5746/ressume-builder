import React from "react";

import "./Title.scss";

interface TitleProps {
  title: string;
  start: string;
  end: string;
}

const Title: React.FC<TitleProps> = (props) => {
  return (
    <div className="role">
      <div className="title">{props.title}</div>
      <div className="date-range">{`${props.start} - ${props.end}`}</div>
    </div>
  );
};

export default Title;
