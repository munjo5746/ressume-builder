import React from "react";

import Resume, { KeyValue } from "../../models/Resume";

import "./Header.scss";
import { Icon } from "@blueprintjs/core";

interface HeaderProps {
  resume: Resume;
}

const Header: React.FC<HeaderProps> = (props) => {
  const biography = props.resume?.sections.find((section) => section.type === "biography");
  return !!biography ? (
    <div className="header">
      <div className="name-role-container">
        <div className="name">{props.resume.name}</div>
        <div className="role">{props.resume.role}</div>
      </div>
      <div className="personal-info">
        {Object.keys(biography.content).map((key) => {
          return (
            <div key={`key-header-info-item-${key}`} className="info-item">
              <Icon icon="link" iconSize={12} />
              <span>
                <a target="_blank" href="munjo5746@gmail.com">
                  {(biography.content as KeyValue)[key] as string}
                </a>
              </span>
            </div>
          );
        })}
      </div>
    </div>
  ) : null;
};

export default Header;
