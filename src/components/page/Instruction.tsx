import React from "react";

import "./Instruction.scss";

const Instruction: React.FC = () => {
  return (
    <div className="instruction">
      <div className="title">Markdown format</div>
      <div># [First name] [Last name]</div>
      <br />
      <div>
        <span>- [Job position 1] </span>
      </div>
      <div>
        <span>- [Job position 2] </span>
      </div>
      <br />
      <div>## Biography</div>
      <div>
        <span>- [Reference name]: </span>
        <span>[Reference url]</span>
      </div>
      <div>
        <span>- [Reference name]: </span>
        <span>[Reference url]</span>
      </div>
      <div>
        <span>- [Reference name]: </span>
        <span>[Reference url]</span>
      </div>
      <br />
      <div>## Skills, [Job position #]</div>
      <div>
        <div>
          <span>- [Skill category]: </span>
          <span>[Skills separated by ,]</span>
        </div>
        <div>
          <span>- [Skill category]: </span>
          <span>[Skills separated by ,]</span>
        </div>
        <div>
          <span>- [Skill category]: </span>
          <span>[Skills separated by ,]</span>
        </div>
      </div>

      <br />

      <div>## Experiences, [Job position #]</div>
      <div>
        <div>### [Company name], [Role], [Start date] - [End date]</div>
        <div>
          <span>- [Achievement]</span> <br />
          <span>- [Achievement]</span> <br />
          <span>- [Achievement]</span>
        </div>
      </div>

      <br />

      <div>## Educations</div>
      <div>
        <div>### [School name], [Major], [Start date] - [End date]</div>
        <div>
          <span>- [Achievement]</span> <br />
          <span>- [Achievement]</span> <br />
          <span>- [Achievement]</span>
        </div>
      </div>

      <div>
        <div className="title">Example</div>
        {/* prettier-ignore */}
        <pre>
              
{`
# [First Name] [Last Name]
- [Role 1]
- [Role 2]

## Biography

- Email: [Your email@whatever.com]
- Github: [Github URL if you have one]
- Gitlab: [Gitlab URL if you have one]
- Blog: [Blog URL if you have one]
- LinkedIn: [Your LinkedIn URL]

## Skills, [Role 1]

- [Skill category]: [List of skills separated by ,]
- Frontend: ReactJs, SASS, Webpack, NPM, .NET framework
- Backend: ExpressJs, Golang, Serverless, AWS lambda
- Database: Postgresql, MSSQL, MongoDB, DynamoDB, Elasticsearch, Entity Framework
- QA: Jest, Cypress, SonarQube
- Programming Languages: Javascript, Typescript, C#, Swift, Rust, Python


## Skills, [Role 2]

- [Skill category]: [List of skills separated by ,]
- Backend: ExpressJs, Golang, Serverless, AWS lambda
- Database: Postgresql, MSSQL, MongoDB, DynamoDB, Elasticsearch, Entity Framework
- QA: Jest, Cypress, SonarQube
- DevOps: Circle CI, CodeBuild, CodePipeline, Bash, Golang
- Programming Languages: Javascript, Typescript, C#, Swift, Rust, Python

## Experiences, [Role 1]

### [Company name 1], [Your role], [Start] - [End]

- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.

### [Company name 2], [Your role], [Start] - [End]

- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.

## Experiences, [Role 2]

### [Company name 1], [Your role], [Start] - [End]

- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.

### [Company name 2], [Your role], [Start] - [End]

- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.

### [Company name 3], [Your role], [Start] - [End]

- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem ex, laoreet quis nibh nec, accumsan hendrerit tortor. Cras eget ligula auctor, consequat metus quis, euismod libero.

## Educations

### [School 1], [Start] - [End]

- [Description 1]
- [Description 2]

### [School 2], [Start] - [End]

- [Description 1]
- [Description 2]
`}
          </pre>
      </div>
    </div>
  );
};

export default Instruction;
