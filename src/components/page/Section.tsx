import React, { ReactNode } from "react";

import "./Section.scss";
interface SectionProps {
  title: string;
  children: ReactNode;
}

const Section: React.FC<SectionProps> = (props) => {
  return (
    <div className="summary section">
      <div className="title">
        {props.title}
        <div className="short-border"></div>
      </div>
      <div className="content">{props.children}</div>
    </div>
  );
};

export default Section;
