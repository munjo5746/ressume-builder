import React from "react";

import "./BulletList.scss";

interface BulletListProps {
  title: string;
  list: string[];
}

const BulletList: React.FC<BulletListProps> = (props) => {
  return (
    <div className="bullet-list">
      <div className="title">{props.title}</div>
      <div className="list">
        <ul>
          {props.list.map((content, idx) => (
            <li key={`key-bullet-list-li-${idx}`}>{content}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default BulletList;
