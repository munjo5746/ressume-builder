import React from "react";

import Header from "./page/Header";

import Section from "./page/Section";
import BulletList from "./page/BulletList";
import Title from "./page/Title";
import Instruction from "./page/Instruction";

import Resume, { ContentTitleBulletList } from "../models/Resume";

import "./Page.scss";

interface PageProps {
  resume?: Resume;
}

interface PageHandlers {
  onClick?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

const Page: React.FC<PageProps & PageHandlers> = (props) => {
  const renderHeader = (resume: Resume) => <Header resume={resume} />;

  const renderSummary = (resume: Resume) => {
    const section = resume.sections.find((section) => section.type === "summary");

    return !!section ? <Section title="summary">{section.content}</Section> : null;
  };

  const renderExperiences = (resume: Resume) => {
    const experiences = resume.sections.find((section) => section.type === "experiences");

    return !!experiences ? (
      <Section title={experiences.type}>
        {(experiences.content as ContentTitleBulletList[]).map((jobTitleBulletList) => {
          const { contentTitle, bulletList } = jobTitleBulletList;
          return (
            <div
              key={`key-experiences-content-title-${contentTitle.title}`}
              className="section-container"
            >
              <div className="horizontal-section">
                <Title
                  title={contentTitle.title}
                  start={contentTitle.period.start}
                  end={contentTitle.period.end}
                />
                <BulletList title={bulletList.title} list={bulletList.list} />
              </div>
            </div>
          );
        })}
      </Section>
    ) : null;
  };

  const renderEducations = (resume: Resume) => {
    const educations = resume.sections.find((section) => section.type === "educations");

    return !!educations ? (
      <Section title={educations.type}>
        {(educations.content as ContentTitleBulletList[]).map((jobTitleBulletList) => {
          const { contentTitle, bulletList } = jobTitleBulletList;

          return (
            <div
              key={`key-educations-content-title-${contentTitle.title}`}
              className="section-container"
            >
              <div className="horizontal-section">
                <Title
                  title={contentTitle.title}
                  start={contentTitle.period.start}
                  end={contentTitle.period.end}
                />
                <BulletList title={bulletList.title} list={bulletList.list} />
              </div>
            </div>
          );
        })}
      </Section>
    ) : null;
  };

  const doesSummarySectionExist = props.resume?.sections.find(
    (section) => section.type === "summary"
  );
  const doesExperiencesSectionExist = props.resume?.sections.find(
    (section) => section.type === "experiences"
  );

  return !!props.resume ? (
    <div className="page A4" onClick={props.onClick}>
      {renderHeader(props.resume)}

      {renderSummary(props.resume)}

      {doesSummarySectionExist && <div className="divider"></div>}

      {renderExperiences(props.resume)}

      {doesExperiencesSectionExist && <div className="divider"></div>}

      {renderEducations(props.resume)}
    </div>
  ) : (
    <Instruction />
  );
};

export default Page;
