# Edward Chung

- Senior Software Engineer
- Devops Engineer

## Biography

- Email: munjo5746@gmail.com
- Github: www.github.com/munjo5746
- Gitlab: https://gitlab.com/munjo5746
- Blog: https://munjo5746.github.io/
- LinkedIn: www.linkedin.com/in/munjo5746/

## Skills, Senior Software Engineer

- Frontend: ReactJs, SASS, Webpack, NPM, .NET framework
- Backend: ExpressJs, Golang, Serverless, AWS lambda
- Database: Postgresql, MSSQL, MongoDB, DynamoDB, Elasticsearch, Entity Framework
- QA: Jest, Cypress, SonarQube
- Programming Languages: Javascript, Typescript, C#, Swift, Rust, Python

## Skills, Devops Engineer

- Backend: ExpressJs, Golang, Serverless, AWS lambda
- Database: Postgresql, MSSQL, MongoDB, DynamoDB, Elasticsearch, Entity Framework
- QA: Jest, Cypress, SonarQube
- DevOps: Circle CI, CodeBuild, CodePipeline, Bash, Golang
- Programming Languages: Javascript, Typescript, C#, Swift, Rust, Python

## Experiences, Senior Software Engineer

### Constellation Agency, Fullstack engineer, Dec 2018 - Present

- Built a consistent and isolated development environment across the team using Docker and docker-compose, considerably improving development pace and process.
- Built CI processes using Circle CI that automates from code commits to deployment of software. This allowed the team to focus more on the software logic and speed up the development.
- Embedded code quality checks in every possible way using githooks, code coverage using Jest, and static code analysis using tools including Sonarqube. This prevented the team from pushing low quality code and drastically lowered the number of bugs.
- Embedded Typescript into a ReactJs project to improve the correctness of code at compile time and help the team navigate a project with hundreds of files more efficiently.
- Built an event-based data pipeline with Golang for checking data integrity, efficiently parsing CSV files to write data into DynamoDB, and triggering AWS lambda function that inserts data into Elasticsearch for availability.

### Cubietek, IOS&Fullstack engineer, Sep 2015 - Dec 2018

- Wrote hundreds of complex SQL queries to retrieve data from single and multiple tables using complex joins to meet software requirements efficiently.
- Built a web server using NodeJs and Socket.io to manage real-time updates in multiple parts of ERP software for a logistics company.
- Wrote and maintained documentation including visual diagrams for non-technical clients to support their understanding of the software.
- Built and maintained an iOS app using Swift.

### Nexhealth, Android developer, May 2014 - Sep 2015

- Analyzed software requirements and helped to plan logic flow ahead of development.
- Wrote reliable Java code with heavy focus on error handling and verbose user feedback.
- Built custom views with smooth animations that met design requirements.

## Experiences, Devops Engineer

### Constellation Agency, Fullstack engineer, Dec 2018 - Present

- Built a consistent and isolated development environment across the team using Docker and docker-compose, considerably improving development pace and process.
- Built CI processes using Circle CI that automates from code commits to deployment of software. This allowed the team to focus more on the software logic and speed up the development.
- Embedded code quality checks in every possible way using githooks, code coverage using Jest, and static code analysis using tools including Sonarqube. This prevented the team from pushing low quality code and drastically lowered the number of bugs.
- Embedded Typescript into a ReactJs project to improve the correctness of code at compile time and help the team navigate a project with hundreds of files more efficiently.
- Built an event-based data pipeline with Golang for checking data integrity, efficiently parsing CSV files to write data into DynamoDB, and triggering AWS lambda function that inserts data into Elasticsearch for availability.

### Cubietek, IOS&Fullstack engineer, Sep 2015 - Dec 2018

- Wrote hundreds of complex SQL queries to retrieve data from single and multiple tables using complex joins to meet software requirements efficiently.
- Built a web server using NodeJs and Socket.io to manage real-time updates in multiple parts of ERP software for a logistics company.
- Wrote and maintained documentation including visual diagrams for non-technical clients to support their understanding of the software.
- Built and maintained an iOS app using Swift.

### Nexhealth, Android developer, May 2014 - Sep 2015

- Analyzed software requirements and helped to plan logic flow ahead of development.
- Wrote reliable Java code with heavy focus on error handling and verbose user feedback.
- Built custom views with smooth animations that met design requirements.

## Educations

### The City College of New York, May 2012 - May 2015

- B.S in Computer Science, Pure Mathematics
- Research Award

### The City College of Technology, May 2010 - May 2012

- Applied Mathematics
- Research Award
