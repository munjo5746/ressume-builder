import React from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

import Page from "./components/Page";
import { parse } from "./parser";
import Resume from "./models/Resume";
import AppHeader from "./components/AppHeader";

import previewIconSvg from "./static/icons/preview.svg";

import "./App.scss";
import { Button, EditableText, NonIdealState } from "@blueprintjs/core";

function App() {
  const [resumes, setResumes] = React.useState<Record<string, Resume>>();
  const [resumeInMarkdown, setResumeInMarkdown] = React.useState<string>();
  const [selectedResume, setSelectedResume] = React.useState<Resume>();
  const [showResumeInput, toggleResumeInput] = React.useState<boolean>(false);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <div className="App">
            <div className="app-header-wrapper">
              <AppHeader
                onEditButtonClick={(e) => {
                  e.preventDefault();

                  toggleResumeInput(true);
                }}
              />

              {showResumeInput && (
                <div className="edit-input-wrapper">
                  <EditableText
                    placeholder={"Add resume in markdown"}
                    multiline={true}
                    minLines={15}
                    maxLines={30}
                    selectAllOnFocus={true}
                    alwaysRenderInput={true}
                    onChange={(value) => {
                      setResumeInMarkdown(value);
                    }}
                    value={resumeInMarkdown}
                  />

                  <div className="button-container">
                    <Button
                      onClick={() => {
                        toggleResumeInput(false);
                      }}
                    >
                      Close
                    </Button>
                    <Button
                      intent="primary"
                      rightIcon="arrow-right"
                      onClick={() => {
                        if (!resumeInMarkdown) return;

                        toggleResumeInput(false);

                        const parsedResumes = parse(resumeInMarkdown);
                        const roles = Object.keys(parsedResumes);
                        if (roles.length > 0) {
                          setSelectedResume(parsedResumes[roles[0]]);
                        }

                        setResumes(parsedResumes);
                      }}
                    >
                      Process
                    </Button>
                  </div>
                </div>
              )}
            </div>
            <div className="body">
              <div className="selection">
                {!resumes && (
                  <NonIdealState
                    icon="slash"
                    title="No resume"
                    description="No resume was processed."
                  />
                )}
                {!!resumes &&
                  Object.keys(resumes).map((role) => (
                    <div
                      key={`key-selection-${role}`}
                      className={`page-wrapper ${role === selectedResume?.role ? "selected" : ""}`}
                    >
                      <Page
                        resume={resumes[role]}
                        onClick={(e) => {
                          e.preventDefault();

                          setSelectedResume(resumes[role]);
                        }}
                      />
                    </div>
                  ))}
              </div>
              <div className="preview">
                <Page resume={selectedResume}></Page>
                {!!resumeInMarkdown && (
                  <div className="preview-menu">
                    <div className="icon-wrapper">
                      <img className="preview-button" src={previewIconSvg} alt="" />
                      <Link to="/print" />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </Route>

        <Route path="/print">
          <div className="print-preview-wrapper">
            <div className="print-preview-menu">
              <div
                className="close-button"
                onClick={(e) => {
                  e.preventDefault();

                  window.history.back();
                }}
              >
                close
              </div>

              <div
                className="print-button"
                onClick={(e) => {
                  e.preventDefault();

                  window.print();
                }}
              >
                print
              </div>
            </div>
            <Page resume={selectedResume} />
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
