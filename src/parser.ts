// const fs = require('fs');

import marked from "marked";
import Resume, {
  BulletList,
  Period,
  ContentTitle,
  ContentTitleBulletList,
  Section,
  SectionType,
} from "./models/Resume";

export const parse = (resumeMarkdown: string) => {
  const lexer = new marked.Lexer();
  const output = lexer.lex(resumeMarkdown);
  const { name, roles } = getNameAndRoles(output) || { name: "-", roles: [] };

  const biography = getBiography(output);
  const educations = getSection(output, "educations");

  const resumes: { [key: string]: Resume } = roles.reduce((acc, role) => {
    const skill = getSkill(output, role);
    const experiences = getSection(output, "experiences", role);

    acc[role] = new Resume({
      name,
      role,
      sections: [biography, skill, experiences, educations],
    });

    return acc;
  }, {} as { [key: string]: Resume });

  return resumes;
};

const getNameAndRoles = (
  args: marked.TokensList
): { name: string; roles: string[] } | undefined => {
  const header = getHeader(args, 1);
  if (!header) throw new Error("#1 header was not found.");

  const roleList = getRange(
    args,
    (item) => isHeading(item) && (item as marked.Tokens.Heading).depth === 1,
    (item) => isList(item)
  );

  if (!roleList || roleList.length === 0) throw new Error("There should be at least one role.");

  // must have one list item before the next header
  if (roleList.length !== 1) throw new Error("There was resume markdown format issue.");

  const name = header.text;
  const roles: string[] = (roleList[0] as marked.Tokens.List).items.map((ele) => ele.text.trim());

  return {
    name,
    roles,
  };
};

const getBiography = (args: marked.TokensList) =>
  new Section({
    type: "biography",
    title: "Biography",
    content: getKeyValueContent(args, "biography"),
  });
const getSkill = (args: marked.TokensList, role: string) => {
  return new Section({
    type: "skills",
    title: "Skills",
    content: getKeyValueContent(args, "skills", role),
  });
};

const getSection = (args: marked.TokensList, sectionType: SectionType, role?: string): Section => {
  const from = (arg: marked.Token) =>
    isHeading(arg) &&
    (arg as marked.Tokens.Heading).depth === 2 &&
    (arg as marked.Tokens.Heading).text.toLowerCase().startsWith(sectionType.toLowerCase()) &&
    (!!role
      ? (arg as marked.Tokens.Heading).text.toLowerCase().indexOf(role.toLowerCase()) !== -1
      : true);
  const during = (arg: marked.Token) =>
    (isHeading(arg) && (arg as marked.Tokens.Heading).depth !== 2) || !isHeading(arg);

  let jobTitleBulletList: ContentTitleBulletList[] = [];
  const range = getRange(args, from, during);
  for (let i = 0; i < range.length; i += 2) {
    const heading = range[i] as marked.Tokens.Heading;

    let contentTitle: ContentTitle;
    if (sectionType === "educations") {
      const [title, dateRange] = heading.text.split(",").map((part) => part.trim());
      const [start, end] = dateRange.split("-").map((part) => part.trim());

      const jobPeriod: Period = {
        start,
        end,
      };

      contentTitle = {
        title,
        period: jobPeriod,
      };
      const list = (range[i + 1] as marked.Tokens.List)?.items.map((ele) => ele.text);
      const bulletList: BulletList = {
        title,
        list,
      };

      const jobTitleBulletListItem: ContentTitleBulletList = {
        contentTitle,
        bulletList,
      };

      jobTitleBulletList.push(jobTitleBulletListItem);
    } else {
      const [title, role, dateRange] = heading.text.split(",").map((part) => part.trim());
      const [start, end] = dateRange.split("-").map((part) => part.trim());

      const jobPeriod: Period = {
        start,
        end,
      };

      contentTitle = {
        title,
        period: jobPeriod,
      };
      const list = (range[i + 1] as marked.Tokens.List)?.items.map((ele) => ele.text);
      const bulletList: BulletList = {
        title: role,
        list,
      };

      const jobTitleBulletListItem: ContentTitleBulletList = {
        contentTitle,
        bulletList,
      };

      jobTitleBulletList.push(jobTitleBulletListItem);
    }
  }

  let title: string = "-";
  switch (sectionType) {
    case "experiences":
      title = "Experiences";
      break;

    case "educations":
      title = "Educations";
      break;
    default:
      break;
  }
  return new Section({
    type: sectionType,
    title,
    content: jobTitleBulletList,
  });
};

const getHeader = (args: marked.TokensList, depth: number, text?: string) => {
  const heading = args.find((arg) =>
    isHeading(arg) && (arg as marked.Tokens.Heading).depth === depth && !!text
      ? (arg as marked.Tokens.Heading).text.toLowerCase() === text.toLowerCase()
      : true
  );

  return !!heading ? (heading as marked.Tokens.Heading) : undefined;
};

const getKeyValueContent = (
  args: marked.TokensList,
  section: SectionType,
  role?: string
): { [key: string]: string } => {
  const checkFunc = (arg: marked.Token) => {
    if (!isHeading(arg)) return false;

    const heading = arg as marked.Tokens.Heading;
    const { depth, text } = heading;

    const textSplit = !!role ? text.split(",").map((txt) => txt.trim()) : null;

    return (
      depth === 2 &&
      text.toLowerCase().startsWith(section.toLowerCase()) &&
      (!!textSplit ? textSplit[textSplit.length - 1] === role : true)
    );
  };

  const headers = args.filter(checkFunc);
  if (headers.length !== 1) {
    throw new Error(`There should be one ${section}.`);
  }

  let data: { [key: string]: string } = {};
  for (let i = 0; i < args.length; i++) {
    const arg = args[i];
    if (!checkFunc(arg) || !isList(args[i + 1])) continue;

    const list = args[i + 1] as marked.Tokens.List;

    // at the next index, it should have data for the "title" section.
    const rows = list.items;
    for (const row of rows) {
      // below handle case like a url with protocol. ex) https://gitlab.com/munjo5746
      const separatorIndex = row.text.indexOf(":");
      const key = row.text.substring(0, separatorIndex);
      const value = row.text.substring(separatorIndex + 1).trim();

      data = Object.assign(data, {
        [key]: value,
      });
    }
  }

  return data;
};

const getRange = (
  list: marked.TokensList,
  from: (item: marked.Token) => boolean,
  during: (item: marked.Token) => boolean
) => {
  let startExtracting = false;
  const subArray = [];
  for (let i = 0; i < list.length; i++) {
    const item = list[i];
    if (from(item)) {
      startExtracting = true;
    } else if (startExtracting && during(item)) {
      subArray.push(item);
    } else if (startExtracting && !during(item)) {
      break;
    }
  }

  return subArray;
};

const isHeading = (arg: marked.Token): boolean => {
  return (arg as marked.Tokens.Heading)?.type === "heading";
};

const isList = (arg: marked.Token): boolean => {
  return ((arg as marked.Tokens.List)?.type as string) === "list";
};
